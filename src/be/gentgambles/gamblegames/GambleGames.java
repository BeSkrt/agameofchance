package be.gentgambles.gamblegames;

import java.util.Random;
import java.util.Scanner;

public class GambleGames {

    public static int coins = Math.abs(20);
    public static int earnings = Math.abs(0);
    public static int loss = Math.abs(0);
    public static int profit = earnings - loss;
    public static Scanner keyboard = new Scanner(System.in);

    public static void main(String[] args) {
        //intro
        System.out.println("*** Welkom bij gokspelen Gent Gambles. ***\n*** U start met 20 coins. Elk spel kost 5 coins om te spelen. ***\n" +
                "*** Met elk spel maakt u kans om meer coins te winnen. ***\n");

        //kies spel
        int chooseGame;
        do {
            System.out.println("*** Geef het nummer in van het spel dat u wilt spelen: ***" + '\t' + "(Coins: " + coins + ")" + '\n');
            System.out.println("+-+-+-+-+-+-+-+-+" + '\n' + "1. Hoger - Lager" + '\n' + "2. Lotto trekking" + '\n' +
                    "3. Slot machine" + '\n' + "4. 21en" + '\n' + "5. Stoppen" + '\n' + "+-+-+-+-+-+-+-+-+" + '\n');
            System.out.print("Nummer spel: ");

            switch (chooseGame = keyboard.nextInt()) {
                case 1:
                    hogerLager();
                    break;
                case 2:
                    lottoTrekking();
                    break;
                case 3:
                    slotMachine();
                    break;
                case 4:
                    blackJack();
                    break;
                case 5:
                    System.out.println("*** Bedankt om te spelen. U heeft in totaal " + coins + "coins, " +
                            "waarbij u een winst heeft van " + profit + "coins. Gefeliciteerd en graag tot de volgende keer. ***");
                    keyboard.close();
                    System.exit(0);
                default:
                    System.out.println("Gelieve een nummer overeenkomstig met het spel op te geven.");
                    break;
            }


        } while (chooseGame != 5);

    }

            //stoppen
            /*if (chooseGame == 5) {
                System.out.println("*** Bedankt om te spelen. U heeft in totaal " + coins + "coins, waarbij u een winst heeft van " + profit + "coins. Gefeliciteerd en graag tot de volgende keer. ***");
                keyboard.close();
                System.exit(0);
                //geen coins meer
            } else if (coins < 5) {
                System.out.println("(" + coins + ")" + "U heeft niet genoeg coins om te spelen. Het programma zal nu afsluiten. Bedankt om te spelen.");*/
                //hoger-lager




//methode hoger - lager
    private static void hogerLager(){
        coins = coins - 5;
        loss = loss + 5;
        System.out.println("*** Hoger - Lager ***" + '\t' + "Coins: " + coins + '\n');
        System.out.println("*** Er wordt een getal gekozen van 1 t.e.m. 47. Indien u het getal raadt in minder dan 5 beurten wint u tot wel 100 coins. Veel succes. ***");
        Random random = new Random();
        int numberToGuess = random.nextInt(47);
        boolean guessed = false;
        int count = 0;

        while (!guessed) {
            System.out.println("Uw gok: ");
            int guess = keyboard.nextInt();
            if (guess < numberToGuess) {
                count++;
                System.out.println("Hoger!");
            } else if (guess > numberToGuess) {
                count++;
                System.out.println("Lager!");
            } else {
                count++;

                if (count == 1) {
                    earnings = 100;
                    coins = coins + 100;
                } else if (count == 2) {
                    earnings = 50;
                    coins = coins + 50;
                } else if (count == 3) {
                    earnings = 20;
                    coins = coins + 20;
                } else if (count == 4) {
                    earnings = 5;
                    coins = coins + 5;
                } else {
                    earnings = 0;
                    coins = coins + 0;
                }
                guessed = true;
                System.out.println("Geraden! U heeft " + count + " keer geraden en u heeft " + earnings + " coins verdiend.");
                System.out.println("Wenst u nogmaals te spelen? (J/N)");
                String jaNee = keyboard.next();
                if (jaNee.equals("J")) {
                    break;
                } else if (jaNee.equals("N")) {
                    break;
                }

            }
        }

    }

    private static void lottoTrekking(){

    }

    private static void slotMachine(){

    }

    private static void blackJack(){

    }

}